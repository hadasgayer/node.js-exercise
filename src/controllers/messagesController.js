const { fstat } = require('fs');

//Import messagess model for database data
messagesModel = require('../models/messagesModel');

exports.getMessagesController = (req, res, next) => {

    const messages = messagesModel.getMessages();
    res.status(200).json(messages);
};

//Return the messages of a specific user
exports.getUserController = (req, res, next) => {
    let username = req.body.user;
    const userMessages = messagesModel.getUserMessages(username);
    res.status(200).json(userMessages);
};

exports.getUnreadController = (req, res, next) => {
    let username = req.body.user;
    const userMessages = messagesModel.getUnreadMessages(username);
    res.status(200).json(userMessages);
};

exports.postMessageController = (req, res, next) => {
    let message = req.body;
    messagesModel.postMessage(message);
    res.status(200).json(message);
};

exports.deleteMessageController = (req, res, next) => {
    let message = req.body;
    messagesModel.deleteMessage(message);
    res.status(200).json(message);
};
