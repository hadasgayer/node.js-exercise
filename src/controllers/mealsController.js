//Import Meals model for database data
mealModel = require('../models/mealModel');

//Menu controller takes the request, response and next object
exports.getMenuController = (req, res, next) => {

    //Get mmenu meals from model (as an array)
    const meals = mealModel.getMeals();

    //Show JSON object of Meals Menu Data
    res.json(meals);
};
